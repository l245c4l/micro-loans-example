package com.example.microloans.business;

import com.example.microloans.domain.loan.LoanProperties;
import org.immutables.value.Value;

@Value.Immutable
public interface ApplyForLoanCommand extends LoanProperties {

}
