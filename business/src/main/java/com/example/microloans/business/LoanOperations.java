package com.example.microloans.business;

import com.example.microloans.domain.loan.ImmutableLoan;
import com.example.microloans.domain.loan.ImmutableLoanCreationProperties;
import com.example.microloans.domain.loan.Loan;
import com.example.microloans.shared.Error;
import com.example.microloans.shared.ErrorCode;
import io.vavr.control.Validation;
import javax.inject.Named;

@Named
public class LoanOperations {

  final LoanRepository loanRepository;

  public LoanOperations(LoanRepository loanRepository) {
    this.loanRepository = loanRepository;
  }

  public Validation<Error, Loan> applyForLoan(final ApplyForLoanCommand command) {

    return Validation.<Error, ApplyForLoanCommand>valid(command)
      .map(c -> ImmutableLoanCreationProperties.builder()
        .from(c)
        .build())
      .flatMap(loanRepository::persist);
  }

  public Validation<Error, Loan> extendLoan(final ExtendLoanCommand extendLoanCommand) {
    return loanRepository.findExisting(extendLoanCommand.loanId())
      .flatMap(this::validateExtendLoanCommand)
      .map(loan -> ImmutableLoan.copyOf(loan)
        .withExtended(true)
      )
      .flatMap(loanRepository::update);
  }

  private Validation<Error, Loan> validateExtendLoanCommand(final Loan loan) {
    if (loan.extended()) {
      return Validation.invalid(ErrorCode.LOAN_TERM_ALREADY_EXTENDED.asError(loan));
    }

    return Validation.valid(loan);
  }
}
