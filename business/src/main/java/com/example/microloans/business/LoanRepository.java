package com.example.microloans.business;

import com.example.microloans.domain.loan.Loan;
import com.example.microloans.domain.loan.LoanCreationProperties;
import com.example.microloans.shared.Error;
import io.vavr.control.Validation;

public interface LoanRepository {

  Validation<Error, Loan> persist(final LoanCreationProperties loanCreationProperties);

  Validation<Error, Loan> update(final Loan loan);

  Validation<Error, Loan> findExisting(final Long id);
}
