package com.example.microloans.business;

import org.immutables.value.Value;

@Value.Immutable
public interface ExtendLoanCommand {

  Long loanId();
}
