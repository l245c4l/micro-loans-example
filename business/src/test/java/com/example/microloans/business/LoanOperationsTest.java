package com.example.microloans.business;

import static com.example.microloans.domain.fixtures.LoanFixtures.someLoan;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;

import com.example.microloans.domain.loan.ImmutableLoan;
import com.example.microloans.domain.loan.ImmutableLoanCreationProperties;
import com.example.microloans.domain.loan.Loan;
import com.example.microloans.shared.Error;
import com.example.microloans.shared.ErrorCode;
import io.vavr.control.Validation;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class LoanOperationsTest {

  private LoanOperations loanOperations;
  private LoanRepository loanRepository;

  @Before
  public void setup() {
    loanRepository = Mockito.mock(LoanRepository.class);
    loanOperations = new LoanOperations(loanRepository);
  }

  @Test
  public void thatLoanCanBeCreated() {
    // given
    final Loan someLoan = someLoan();

    final ApplyForLoanCommand command = ImmutableApplyForLoanCommand.builder()
      .from(someLoan)
      .build();

    Mockito.when(loanRepository.persist(any())).thenReturn(
      Validation.valid(someLoan)
    );

    // when
    final Validation<Error, Loan> result = loanOperations.applyForLoan(command);

    // then
    assertThat(result).isNotEmpty();
    assertThat(result.get()).isEqualTo(someLoan);

    Mockito.verify(loanRepository).persist(
      ImmutableLoanCreationProperties.builder()
        .from(someLoan)
        .build()
    );
  }

  @Test
  public void thatLoanApplicationFailsWhenRepositoryPersistingFails() {
    // given
    final Loan someLoan = someLoan();

    final ApplyForLoanCommand command = ImmutableApplyForLoanCommand.builder()
      .from(someLoan)
      .build();

    final Error expectedError = ErrorCode.DATA_ACCESS_EXCEPTION.asError();

    Mockito.when(loanRepository.persist(any())).thenReturn(
      Validation.invalid(expectedError)
    );

    // when
    final Validation<Error, Loan> result = loanOperations.applyForLoan(command);

    // then
    assertThat(result).isEmpty();
    assertThat(result.getError()).isEqualTo(expectedError);
  }

  @Test
  public void thatLoanCanBeExtended() {
    // given
    final ImmutableLoan someLoan = someLoan();

    final ExtendLoanCommand command = ImmutableExtendLoanCommand.builder()
      .loanId(someLoan.id())
      .build();

    Mockito.when(loanRepository.findExisting(any())).thenReturn(
      Validation.valid(someLoan)
    );

    Mockito.when(loanRepository.update(any()))
      .thenAnswer(invocation -> Validation.valid(invocation.getArgument(0)));

    // when
    final Validation<Error, Loan> result = loanOperations.extendLoan(command);

    // then
    final Loan expectedLoan = someLoan.withExtended(true);

    assertThat(result).isNotEmpty();
    assertThat(result.get()).isEqualTo(expectedLoan);

    Mockito.verify(loanRepository).findExisting(someLoan.id());
    Mockito.verify(loanRepository).update(expectedLoan);
  }

  @Test
  public void thatCannotBeExtendedTwice() {
    // given
    final Loan someExtendedLoan = someLoan().withExtended(true);

    final ExtendLoanCommand command = ImmutableExtendLoanCommand.builder()
      .loanId(someExtendedLoan.id())
      .build();

    Mockito.when(loanRepository.findExisting(any())).thenReturn(
      Validation.valid(someExtendedLoan)
    );

    // when
    final Validation<Error, Loan> result = loanOperations.extendLoan(command);

    // then
    assertThat(result).isEmpty();
    assertThat(result.getError()).isEqualTo(ErrorCode.LOAN_TERM_ALREADY_EXTENDED.asError(someExtendedLoan));

    Mockito.verify(loanRepository, Mockito.never()).update(any());
  }

  @Test
  public void thatNonExistingLoanCannotBeExtended() {
    // given
    final ExtendLoanCommand command = ImmutableExtendLoanCommand.builder()
      .loanId(1L)
      .build();

    final Error expectedError = ErrorCode.DATA_ACCESS_EXCEPTION.asError();

    Mockito.when(loanRepository.findExisting(any())).thenReturn(
      Validation.invalid(expectedError)
    );

    // when
    final Validation<Error, Loan> result = loanOperations.extendLoan(command);

    // then
    assertThat(result).isEmpty();
    assertThat(result.getError()).isEqualTo(expectedError);

    Mockito.verify(loanRepository, Mockito.never()).update(any());
  }

  @Test
  public void thatLoanExtensionFailsWhenRepositoryUpdatingFails() {
    // given
    final Loan someLoan = someLoan();

    final ExtendLoanCommand command = ImmutableExtendLoanCommand.builder()
      .loanId(someLoan.id())
      .build();

    final Error expectedError = ErrorCode.DATA_ACCESS_EXCEPTION.asError();

    Mockito.when(loanRepository.findExisting(any())).thenReturn(
      Validation.valid(someLoan)
    );

    Mockito.when(loanRepository.update(any())).thenReturn(
      Validation.invalid(expectedError)
    );

    // when
    final Validation<Error, Loan> result = loanOperations.extendLoan(command);

    // then
    assertThat(result).isEmpty();
    assertThat(result.getError()).isEqualTo(expectedError);
  }

}
