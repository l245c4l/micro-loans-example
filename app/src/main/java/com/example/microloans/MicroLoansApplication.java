package com.example.microloans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroLoansApplication {

  public static void main(String[] args) {
    SpringApplication.run(MicroLoansApplication.class, args);
  }
}
