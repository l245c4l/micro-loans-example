package com.example.microloans;

import com.example.microloans.persitence.InMemoryLoanRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.internal.mapping.Jackson2Mapper;
import io.restassured.specification.RequestSpecification;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public abstract class CommonFunctionalTest {

  private static final String HOST = "localhost";
  private static final String HTTP = "http";

  @LocalServerPort
  private int serverPort;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private InMemoryLoanRepository repository;

  @Before
  public void before() {
    RestAssured.objectMapper(new Jackson2Mapper((cls, charset) ->
      objectMapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, true)
    ));
    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    repository.cleanDatabase();
  }

  protected RequestSpecification requestSpecification() {
    return RestAssured
      .given()
      .log().all()
      .port(serverPort)
      .accept(ContentType.JSON)
      .contentType(ContentType.JSON);
  }

  String buildCreatedUri(final String apiUrl, final String id) {
    return UriComponentsBuilder
      .newInstance()
      .scheme(HTTP)
      .host(HOST)
      .port(serverPort)
      .path(apiUrl)
      .pathSegment(id)
      .build()
      .toUriString();
  }
}

