package com.example.microloans;

import com.example.microloans.api.dto.error.ErrorResponse;
import com.example.microloans.api.dto.loan.ApplyForLoanRequest;
import com.example.microloans.api.dto.loan.LoanResponse;
import com.example.microloans.api.rest.loan.LoanController;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.http.HttpStatus;

public interface LoanFunctionalTestSupport {

    RequestSpecification requestSpecification();

    default LoanResponse applyForLoan(
      final ApplyForLoanRequest applyForLoanRequest
    ) {
      return requestSpecification()
        .body(applyForLoanRequest)
        .when()
        .post(LoanController.API_V1_LOANS)
        .then()
        .log().all()
        .statusCode(HttpStatus.CREATED.value())
        .contentType(ContentType.JSON)
        .and()
        .extract()
        .body()
        .as(LoanResponse.class);
    }

  default LoanResponse extendLoan(
    final Long loanId
  ) {
    return requestSpecification()
      .when()
      .post(LoanController.API_V1_LOAN_EXTEND, loanId)
      .then()
      .log().all()
      .statusCode(HttpStatus.OK.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(LoanResponse.class);
  }

  default ErrorResponse extendLoanExpectingError(
    final Long loanId
  ) {
    return requestSpecification()
      .when()
      .post(LoanController.API_V1_LOAN_EXTEND, loanId)
      .then()
      .log().all()
      .statusCode(HttpStatus.BAD_REQUEST.value())
      .contentType(ContentType.JSON)
      .and()
      .extract()
      .body()
      .as(ErrorResponse.class);
  }
}
