package com.example.microloans;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.microloans.api.dto.error.ErrorDto;
import com.example.microloans.api.dto.error.ErrorResponse;
import com.example.microloans.api.dto.loan.ApplyForLoanRequest;
import com.example.microloans.api.dto.loan.ImmutableApplyForLoanRequest;
import com.example.microloans.api.dto.loan.LoanResponse;
import com.example.microloans.domain.loan.Loan;
import com.example.microloans.shared.ErrorCode;
import io.restassured.specification.RequestSpecification;
import io.vavr.collection.List;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import org.junit.Test;

public class LoanFunctionalTest extends CommonFunctionalTest implements LoanFunctionalTestSupport {

  @Override
  public RequestSpecification requestSpecification() {
    return super.requestSpecification();
  }

  @Test
  public void thatLoanCanBeCreated() {
    // given
    final ApplyForLoanRequest request = ImmutableApplyForLoanRequest.builder()
      .amount(100_00L)
      .initialTerm(Duration.parse("PT360H"))
      .build();

    // when
    final LoanResponse response = applyForLoan(request);

    // then
    assertThat(response.amount()).isEqualTo(request.amount());
    assertThat(response.dueDate())
      .isEqualTo(response.createdAt().truncatedTo(ChronoUnit.DAYS).plus(request.initialTerm()));
    assertThat(response.interest()).isEqualTo(10_00L);
  }

  @Test
  public void thatLoanCanBeExtended() {
    // given
    final ApplyForLoanRequest request = ImmutableApplyForLoanRequest.builder()
      .amount(100_00L)
      .initialTerm(Duration.parse("PT360H"))
      .build();

    final LoanResponse loanResponse = applyForLoan(request);

    // when
    final LoanResponse extendedLoanResponse = extendLoan(loanResponse.id());

    // then
    assertThat(extendedLoanResponse.amount()).isEqualTo(request.amount());
    assertThat(extendedLoanResponse.dueDate())
      .isEqualTo(loanResponse
        .createdAt()
        .truncatedTo(ChronoUnit.DAYS)
        .plus(request.initialTerm())
        .plus(Loan.TERM_EXTENSION)
      );
    assertThat(extendedLoanResponse.interest()).isEqualTo(10_00L);
  }

  @Test
  public void thatLoanCannotBeExtendedTwice() {
    // given
    final ApplyForLoanRequest request = ImmutableApplyForLoanRequest.builder()
      .amount(100_00L)
      .initialTerm(Duration.parse("PT360H"))
      .build();

    final LoanResponse loanResponse = applyForLoan(request);

    extendLoan(loanResponse.id());

    // when
    final ErrorResponse extendedLoanErrorResponse = extendLoanExpectingError(loanResponse.id());

    // then
    assertThat(
      List.ofAll(extendedLoanErrorResponse.errors())
        .map(ErrorDto::code))
      .containsExactly(ErrorCode.LOAN_TERM_ALREADY_EXTENDED.asError().code().toString());
  }

}
