package com.example.microloans.shared;

import org.immutables.value.Value;

@Value.Immutable
public interface Error {

  @Value.Parameter
  ErrorCode code();

  @Value.Parameter
  String message();
}
