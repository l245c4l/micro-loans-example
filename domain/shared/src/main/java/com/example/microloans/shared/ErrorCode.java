package com.example.microloans.shared;

import java.text.MessageFormat;
import java.util.Arrays;

public enum ErrorCode {

  MEDIA_TYPE_NOT_SUPPORTED("Supplied media type is not supported."),
  REQUEST_NOT_READABLE("Unexpected error occurred while converting the request."),
  MALFORMED_PATCHED_ENTITY("Malformed patched entity"),
  INTERNAL_SERVER_ERROR("Unexpected error occurred while processing the request."),

  PARAMETER_MUST_BE_POSITIVE("Parameter {0} must be positive. Current: {1}."),

  DATA_ACCESS_EXCEPTION("Database access failed"),
  INVALID_LOAN_AMOUNT("Loan amount {0} must be within {1} - {2}"),
  INVALID_LOAN_TERM("Loan term {0} must be within {1} - {2}"),
  INVALID_LOAN_CREATED_AT("Loan with maximum amount ({0}) cannot be created during: {1} - {2}"),
  LOAN_DOES_NOT_EXIST("Loan with id {0} does not exist"),
  LOAN_TERM_ALREADY_EXTENDED("Term of the loan {0} is already extended");

  final String message;

  ErrorCode(final String message) {
    this.message = message;
  }

  public Error asError(final Object... params) {
    final Object[] quotedParams = Arrays.stream(params)
      .map(param -> "'" + param + "'")
      .toArray(String[]::new);

    return ImmutableError.builder()
      .code(this)
      .message(MessageFormat.format(message, quotedParams))
      .build();
  }

  public String getMessage() {
    return message;
  }
}
