package com.example.microloans.shared;

public interface WithId<T> {
  T id();
}
