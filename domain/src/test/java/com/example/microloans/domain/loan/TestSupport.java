package com.example.microloans.domain.loan;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import com.example.microloans.domain.error.BuilderException;
import com.example.microloans.shared.Error;
import com.example.microloans.shared.ErrorCode;
import org.assertj.core.api.IterableAssert;
import org.assertj.core.api.ThrowableAssert;

public interface TestSupport {

  default IterableAssert<ErrorCode> assertThatErrorCodes(final ThrowableAssert.ThrowingCallable callable) {
    // when
    final Throwable thrown = catchThrowable(callable);

    // then
    assertThat(thrown).isInstanceOf(BuilderException.class);
    return assertThat(BuilderException.class.cast(thrown).errors().map(Error::code));
  }

}
