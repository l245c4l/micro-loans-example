package com.example.microloans.domain.loan;

import static com.example.microloans.domain.fixtures.LoanFixtures.someLoan;
import static org.assertj.core.api.Assertions.assertThat;

import com.example.microloans.domain.TimeProvider;
import com.example.microloans.shared.ErrorCode;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class LoanTest implements TestSupport {

  @ParameterizedTest
  @MethodSource("invalidLoanAmounts")
  void thatLoanWithInvalidAmountCannotBeCreated(final Long amount) {
    assertThatErrorCodes(() -> someLoan().withAmount(amount));
  }

  private static Stream<Arguments> invalidLoanAmounts() {
    return Stream.of(
      Arguments.of(-1L),
      Arguments.of(0L),
      Arguments.of(Loan.MIN_LOAN_AMOUNT - 1L),
      Arguments.of(Loan.MAX_LOAN_AMOUNT + 1L)
    );
  }

  @ParameterizedTest
  @MethodSource("validLoanAmounts")
  void thatLoanWithValidAmountCanBeCreated(final Long amount) {
    someLoan().withAmount(amount);
  }

  private static Stream<Arguments> validLoanAmounts() {
    return Stream.of(
      Arguments.of(Loan.MIN_LOAN_AMOUNT),
      Arguments.of(Loan.MIN_LOAN_AMOUNT + 1L),
      Arguments.of(Loan.MAX_LOAN_AMOUNT)
    );
  }


  @ParameterizedTest
  @MethodSource("invalidLoanTermDurations")
  void thatLoanWithInvalidDurationCannotBeCreated(final Duration duration) {
    assertThatErrorCodes(() -> someLoan().withInitialTerm(duration));
  }

  private static Stream<Arguments> invalidLoanTermDurations() {
    return Stream.of(
      Arguments.of(Duration.ofDays(-1)),
      Arguments.of(Loan.MIN_LOAN_TERM.minusDays(1)),
      Arguments.of(Loan.MAX_LOAN_TERM.plusDays(1)),
      Arguments.of(Duration.ofDays(0))
    );
  }

  @ParameterizedTest
  @MethodSource("validLoanTermDurations")
  void thatLoanWithValidDurationCanBeCreated(final Duration duration) {
    someLoan().withInitialTerm(duration);
  }

  private static Stream<Arguments> validLoanTermDurations() {
    return Stream.of(
      Arguments.of(Loan.MIN_LOAN_TERM),
      Arguments.of(Loan.MIN_LOAN_TERM.plusDays(1)),
      Arguments.of(Loan.MAX_LOAN_TERM)
    );
  }


  @ParameterizedTest
  @MethodSource("invalidLoanCreatedAt")
  void thatLoanWithInvalidCreatedAtCannotBeCreated(final ZonedDateTime createdAt) {
    assertThatErrorCodes(() ->
      someLoan()
        .withAmount(Loan.MAX_LOAN_AMOUNT)
        .withCreatedAt(createdAt)
    ).containsExactly(ErrorCode.INVALID_LOAN_CREATED_AT);
  }

  private static Stream<Arguments> invalidLoanCreatedAt() {
    return Stream.of(
      Arguments.of(TimeProvider.zonedNow().truncatedTo(ChronoUnit.DAYS).withHour(0)),
      Arguments.of(TimeProvider.zonedNow().truncatedTo(ChronoUnit.DAYS).withHour(1)),
      Arguments.of(TimeProvider.zonedNow().truncatedTo(ChronoUnit.DAYS).withHour(6))
    );
  }

  @ParameterizedTest
  @MethodSource("validLoanCreatedAt")
  void thatLoanWithValidCreatedAtCanBeCreated(final ZonedDateTime createdAt) {
    someLoan()
      .withAmount(Loan.MAX_LOAN_AMOUNT)
      .withCreatedAt(createdAt);
  }

  private static Stream<Arguments> validLoanCreatedAt() {
    return Stream.of(
      Arguments.of(TimeProvider.zonedNow().truncatedTo(ChronoUnit.DAYS).withHour(0).minusHours(1)),
      Arguments.of(TimeProvider.zonedNow().truncatedTo(ChronoUnit.DAYS).withHour(6).plusHours(1))
    );
  }

  @Test
  void thatLoanDueDateForNotExtendedTermIsCorrect() {
    // given
    final ZonedDateTime createdAt = TimeProvider.zonedNow();
    final Duration termDuration = Duration.ofDays(15L);

    // when, then
    assertThat(
      someLoan()
        .withExtended(false)
        .withCreatedAt(createdAt)
        .withInitialTerm(termDuration)
        .dueDate()
    ).isEqualTo(createdAt.truncatedTo(ChronoUnit.DAYS).plus(termDuration));
  }

  @Test
  void thatLoanDueDateForExtendedTermIsCorrect() {
    // given
    final ZonedDateTime createdAt = TimeProvider.zonedNow();
    final Long termDuration = 15L;

    // when, then
    assertThat(
      someLoan()
        .withCreatedAt(createdAt)
        .withInitialTerm(Duration.ofDays(termDuration))
        .withExtended(true)
        .dueDate()
    ).isEqualTo(createdAt.plusDays(termDuration).truncatedTo(ChronoUnit.DAYS).plus(Loan.TERM_EXTENSION));
  }

  @Test
  void thatLoanInterestIsCorrect() {
    assertThat(someLoan().interestPercent()).isEqualTo(10L);
    assertThat(someLoan().withAmount(1000_00L).interest()).isEqualTo(100_00L);
  }

}
