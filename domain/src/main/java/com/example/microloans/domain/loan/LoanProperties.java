package com.example.microloans.domain.loan;

import java.time.Duration;
import java.time.ZonedDateTime;

public interface LoanProperties {

  ZonedDateTime createdAt();

  Long amount();

  Duration initialTerm();
}
