package com.example.microloans.domain.error;

import com.example.microloans.shared.Error;
import io.vavr.collection.List;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(depluralize = true)
public abstract class BuilderException extends RuntimeException {

  public abstract List<Error> errors();

  /**
   * Returns a bad request exception carrying given list of validation errors.
   */
  public static BuilderException of(final Iterable<Error> errors) {
    return ImmutableBuilderException.builder()
      .errors(List.ofAll(errors))
      .build();
  }

  /**
   * Returns a bad request exception carrying specified validation error.
   */
  public static BuilderException of(final Error error) {
    return of(List.of(error));
  }

  @Override
  public abstract String toString();

}
