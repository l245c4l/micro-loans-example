package com.example.microloans.domain;

import com.example.microloans.domain.error.BuilderException;
import com.example.microloans.shared.ErrorCode;

public interface ValidationSupport {

  default void ensurePositive(final Long number, final String fieldName) {
    if (number < 0) {
      throw BuilderException.of(
        ErrorCode.PARAMETER_MUST_BE_POSITIVE.asError(fieldName, number.toString())
      );
    }
  }
}
