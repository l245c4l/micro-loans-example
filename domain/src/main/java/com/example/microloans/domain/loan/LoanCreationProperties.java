package com.example.microloans.domain.loan;

import org.immutables.value.Value;

@Value.Immutable
public interface LoanCreationProperties extends LoanProperties {

  @Value.Derived
  default boolean extended() {
    return false;
  }
}
