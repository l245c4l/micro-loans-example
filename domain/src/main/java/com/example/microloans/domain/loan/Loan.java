package com.example.microloans.domain.loan;

import com.example.microloans.domain.ValidationSupport;
import com.example.microloans.domain.error.BuilderException;
import com.example.microloans.shared.ErrorCode;
import com.example.microloans.shared.WithId;
import io.vavr.control.Option;
import java.time.Duration;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import org.immutables.value.Value;

@Value.Immutable
public interface Loan extends LoanProperties, WithId<Long>, ValidationSupport {


  Long MIN_LOAN_AMOUNT = 100_00L;
  Long MAX_LOAN_AMOUNT = 10000_00L;

  Duration MIN_LOAN_TERM = Duration.ofDays(10);
  Duration MAX_LOAN_TERM = Duration.ofDays(300);
  Duration TERM_EXTENSION = Duration.ofDays(30);

  LocalTime _00_00 = LocalTime.of(0, 0);
  LocalTime _06_00 = LocalTime.of(6, 0);

  Long INTEREST_PERCENT = 10L;

  Boolean extended();

  @Value.Derived
  default ZonedDateTime dueDate() {
    return createdAt().truncatedTo(ChronoUnit.DAYS).plus(extendedTerm().getOrElse(initialTerm()));
  }

  @Value.Derived
  default Long interest() {
    return amount() * INTEREST_PERCENT / 100L;
  }

  @Value.Derived
  default Long interestPercent() {
    return INTEREST_PERCENT;
  }

  @Value.Derived
  default Option<Duration> extendedTerm() {
    return extended() ? Option.some(initialTerm().plus(TERM_EXTENSION)) : Option.none();
  }

  default boolean isAmountValid() {
    return amount() < MIN_LOAN_AMOUNT || amount() > MAX_LOAN_AMOUNT;
  }

  default boolean isTermValid() {
    return initialTerm().compareTo(MIN_LOAN_TERM) < 0 || initialTerm().compareTo(MAX_LOAN_TERM) > 0;
  }

  default boolean isCreatedAtValid() {
    return (
      amount().equals(MAX_LOAN_AMOUNT)
      && createdAt().toLocalTime().compareTo(Loan._00_00) >= 0
      && createdAt().toLocalTime().compareTo(Loan._06_00) <= 0
    );
  }

  @Value.Check
  default void checkInterest() {
    ensurePositive(interestPercent(), "interestPercent");
  }

  @Value.Check
  default void checkAmount() {
    ensurePositive(amount(), "amount");

    if (isAmountValid()) {
      throw BuilderException.of(
        ErrorCode.INVALID_LOAN_AMOUNT.asError(
          amount(),
          MIN_LOAN_AMOUNT,
          MAX_LOAN_AMOUNT
        )
      );
    }
  }

  @Value.Check
  default void checkTerm() {
    if (isTermValid()) {
      throw BuilderException.of(
        ErrorCode.INVALID_LOAN_TERM.asError(
          initialTerm(),
          MIN_LOAN_TERM,
          MAX_LOAN_TERM
        )
      );
    }
  }

  @Value.Check
  default void checkCreatedAt() {
    if (isCreatedAtValid()) {
      throw BuilderException.of(
        ErrorCode.INVALID_LOAN_CREATED_AT.asError(
          amount(),
          _00_00,
          _06_00
        )
      );
    }
  }

}
