package com.example.microloans.domain;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class TimeProvider {

  private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ISO_INSTANT;
  private static final DateTimeFormatter LOCAL_DATE_FORMAT = DateTimeFormatter.ISO_DATE;

  private static final ZoneId ZONE_ID = TimeZone.getTimeZone("UTC").toZoneId();

  private TimeProvider() {
  }

  public static ZoneId zoneId() {
    return ZONE_ID;
  }

  public static ZonedDateTime zonedNow() {
    return ZonedDateTime.now(ZONE_ID);
  }

  public static DateTimeFormatter dateFormat() {
    return DATE_FORMAT;
  }

  public static DateTimeFormatter localDateFormat() {
    return LOCAL_DATE_FORMAT;
  }

  public static ZonedDateTime toZoned(final Instant instant) {
    return ZonedDateTime.ofInstant(instant, zoneId());
  }
}
