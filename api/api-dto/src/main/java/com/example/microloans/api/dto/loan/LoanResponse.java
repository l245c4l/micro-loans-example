package com.example.microloans.api.dto.loan;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.ZonedDateTime;
import org.immutables.value.Value;

@Value.Immutable
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonSerialize(as = ImmutableLoanResponse.class)
@JsonDeserialize(as = ImmutableLoanResponse.class)
public interface LoanResponse {

  Long id();

  Long amount();

  Long interest();

  ZonedDateTime createdAt();

  ZonedDateTime dueDate();
}
