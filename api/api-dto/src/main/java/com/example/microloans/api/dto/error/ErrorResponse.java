package com.example.microloans.api.dto.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.List;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableErrorResponse.class)
@JsonDeserialize(as = ImmutableErrorResponse.class)
@Value.Style(jdkOnly = true) // skip using guava collections for deserialization
public interface ErrorResponse {

  @JsonProperty("errors")
  List<ErrorDto> errors();

}
