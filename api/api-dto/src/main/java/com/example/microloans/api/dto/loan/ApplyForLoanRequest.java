package com.example.microloans.api.dto.loan;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.Duration;
import org.immutables.value.Value;

@Value.Immutable
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonSerialize(as = ImmutableApplyForLoanRequest.class)
@JsonDeserialize(as = ImmutableApplyForLoanRequest.class)
public interface ApplyForLoanRequest {

  Long amount();

  Duration initialTerm();
}
