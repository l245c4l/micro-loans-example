package com.example.microloans.api.dto.error;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableErrorDto.class)
@JsonDeserialize(as = ImmutableErrorDto.class)
public interface ErrorDto {

  @JsonProperty("code")
  String code();

  @JsonProperty("message")
  String message();
}
