package com.example.microloans.api.dto.error;

import com.example.microloans.shared.Error;
import java.util.Collections;
import java.util.List;
import org.immutables.value.Value;

@Value.Immutable
@Value.Style(depluralize = true)
public abstract class BadRequestException extends RuntimeException {

  public abstract List<Error> errors();

  /** Returns a bad request exception carrying given list of error details. */
  public static BadRequestException of(final Iterable<Error> errors) {
    return ImmutableBadRequestException.builder()
      .addAllErrors(errors)
      .build();
  }

  /** Returns a bad request exception carrying specified error detail. */
  public static BadRequestException of(final Error error) {
    return of(Collections.singletonList(error));
  }

  @Override
  public abstract String toString();
}
