package com.example.microloans.api.rest.loan;

import com.example.microloans.api.dto.loan.ApplyForLoanRequest;
import com.example.microloans.api.rest.CommonController;
import com.example.microloans.business.ExtendLoanCommand;
import com.example.microloans.business.ImmutableExtendLoanCommand;
import com.example.microloans.business.LoanOperations;
import com.example.microloans.shared.Error;
import io.vavr.control.Validation;
import java.util.function.Function;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoanController extends CommonController {

  public static final String API_V1_LOANS = API_V1 + "/loan";
  public static final String API_V1_LOAN = API_V1_LOANS + "/{loanId}";
  public static final String API_V1_LOAN_EXTEND = API_V1_LOAN + "/extend";

  final LoanOperations loanOperations;

  public LoanController(LoanOperations loanOperations) {
    this.loanOperations = loanOperations;
  }

  @PostMapping(API_V1_LOANS)
  ResponseEntity applyForLoan(final @RequestBody ApplyForLoanRequest request) {
    return Validation.<Error, ApplyForLoanRequest>valid(request)
      .map(LoanDtoMapper::fromRequest)
      .flatMap(loanOperations::applyForLoan)
      .mapError(CommonController::toErrorResponse)
      .map(LoanDtoMapper::toResponse)
      .map(dto -> toCreatedResponse(dto, dto.id()))
      .fold(Function.identity(), Function.identity());
  }

  @PostMapping(API_V1_LOAN_EXTEND)
  ResponseEntity extendLoan(final @PathVariable Long loanId) {
    return Validation.<Error, ExtendLoanCommand>valid(
      ImmutableExtendLoanCommand.builder()
        .loanId(loanId)
        .build()
    )
      .flatMap(loanOperations::extendLoan)
      .mapError(CommonController::toErrorResponse)
      .map(LoanDtoMapper::toResponse)
      .map(ResponseEntity::ok)
      .fold(Function.identity(), Function.identity());
  }
}
