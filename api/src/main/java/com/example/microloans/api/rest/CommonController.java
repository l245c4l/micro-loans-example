package com.example.microloans.api.rest;

import com.example.microloans.api.dto.error.ErrorResponse;
import com.example.microloans.api.dto.error.ImmutableErrorDto;
import com.example.microloans.api.dto.error.ImmutableErrorResponse;
import com.example.microloans.shared.Error;
import io.vavr.collection.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public abstract class CommonController {

  public static final String API_V1 = "/api/v1";

  protected static ResponseEntity<ErrorResponse> toErrorResponse(final Error error) {
    return toErrorResponse(List.of(error), HttpStatus.BAD_REQUEST);
  }

  protected static ResponseEntity<ErrorResponse> toErrorResponse(final List<Error> errorList) {
    return toErrorResponse(errorList, HttpStatus.BAD_REQUEST);
  }

  static ResponseEntity<ErrorResponse> toErrorResponse(final Error error, final HttpStatus status) {
    return toErrorResponse(List.of(error), status);
  }

  static ResponseEntity<ErrorResponse> toErrorResponse(final List<Error> errorList, final HttpStatus status) {
    return ResponseEntity.status(status)
      .body(ImmutableErrorResponse.builder()
        .errors(
          errorList.map(e -> ImmutableErrorDto.builder()
            .message(e.message())
            .code(e.code().name())
            .build()))
        .build());
  }

  protected ResponseEntity toCreatedResponse(final Object responseBody) {
    return ResponseEntity.created(
      ServletUriComponentsBuilder.fromCurrentRequestUri()
        .build()
        .toUri()
    ).body(responseBody);
  }

  protected ResponseEntity toCreatedResponse(final Object responseBody, final Object responseId) {
    return ResponseEntity.created(
      ServletUriComponentsBuilder.fromCurrentRequestUri()
        .pathSegment(responseId.toString())
        .build()
        .toUri()
    ).body(responseBody);
  }
}

