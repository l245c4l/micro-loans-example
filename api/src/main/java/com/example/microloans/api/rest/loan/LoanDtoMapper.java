package com.example.microloans.api.rest.loan;

import com.example.microloans.api.dto.loan.ApplyForLoanRequest;
import com.example.microloans.api.dto.loan.ImmutableLoanResponse;
import com.example.microloans.api.dto.loan.LoanResponse;
import com.example.microloans.business.ApplyForLoanCommand;
import com.example.microloans.business.ImmutableApplyForLoanCommand;
import com.example.microloans.domain.TimeProvider;
import com.example.microloans.domain.loan.Loan;

public interface LoanDtoMapper {

  static ApplyForLoanCommand fromRequest(final ApplyForLoanRequest request) {
    return ImmutableApplyForLoanCommand.builder()
      .amount(request.amount())
      .createdAt(TimeProvider.zonedNow())
      .initialTerm(request.initialTerm())
      .build();
  }

  static LoanResponse toResponse(final Loan loan) {
    return ImmutableLoanResponse.builder()
      .id(loan.id())
      .amount(loan.amount())
      .createdAt(loan.createdAt())
      .dueDate(loan.dueDate())
      .interest(loan.interest())
      .build();
  }
}
