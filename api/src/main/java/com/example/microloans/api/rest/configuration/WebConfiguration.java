package com.example.microloans.api.rest.configuration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.ZonedDateTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatterBuilder;

import static com.example.microloans.domain.TimeProvider.dateFormat;
import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

  @Bean
  public Jackson2ObjectMapperBuilder jacksonBuilder() {
    return new Jackson2ObjectMapperBuilder()
      .featuresToDisable(
        SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
        DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE,
        DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
      .findModulesViaServiceLoader(true)
      .serializers(
        localTimeSerializer(),
        zonedDateTimeSerializer()
      );
  }

  private JsonSerializer<LocalTime> localTimeSerializer() {
    return new LocalTimeSerializer(new DateTimeFormatterBuilder()
      .appendValue(HOUR_OF_DAY, 2)
      .appendLiteral(':')
      .appendValue(MINUTE_OF_HOUR, 2)
      .toFormatter()
    );
  }

  private JsonSerializer<ZonedDateTime> zonedDateTimeSerializer() {
    return new ZonedDateTimeSerializer(dateFormat());
  }
}
