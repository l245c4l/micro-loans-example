package com.example.microloans.api.rest;

import com.example.microloans.api.dto.error.BadRequestException;
import com.example.microloans.api.dto.error.ErrorResponse;
import com.example.microloans.domain.error.BuilderException;
import com.example.microloans.shared.ErrorCode;
import com.example.microloans.shared.ImmutableError;
import io.vavr.collection.List;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.slf4j.LoggerFactory.getLogger;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger log = getLogger(ControllerExceptionHandler.class);

  private static void log(Exception exception) {
    log.error("Request couldn't be processed", exception);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
    HttpMessageNotReadableException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    if (exception.getRootCause() instanceof BadRequestException) {
      final BadRequestException badRequestException = (BadRequestException) exception.getRootCause();
      assert badRequestException != null;
      return widen(handleBadRequestException(badRequestException));
    }

    log(exception);
    final ImmutableError error = ImmutableError.builder()
      .code(ErrorCode.REQUEST_NOT_READABLE)
      .message(exception.getMessage())
      .build();

    return widen(CommonController.toErrorResponse(List.of(error)));
  }

  @Override
  protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
    HttpMediaTypeNotSupportedException exception,
    HttpHeaders headers,
    HttpStatus status,
    WebRequest request
  ) {
    log(exception);
    final ImmutableError error = ImmutableError.builder()
      .code(ErrorCode.MEDIA_TYPE_NOT_SUPPORTED)
      .message(exception.getMessage())
      .build();

    return widen(CommonController.toErrorResponse(List.of(error), status));
  }

  @ExceptionHandler(BuilderException.class)
  private ResponseEntity<ErrorResponse> handleBuilderException(BuilderException exception) {
    log(exception);
    return CommonController.toErrorResponse(exception.errors());
  }

  @ExceptionHandler(BadRequestException.class)
  private ResponseEntity<ErrorResponse> handleBadRequestException(BadRequestException exception) {
    log(exception);
    return CommonController.toErrorResponse(List.ofAll(exception.errors()));
  }

  @ExceptionHandler(HttpMessageConversionException.class)
  private ResponseEntity<ErrorResponse> handleImmutableDtoBuilderException(HttpMessageConversionException exception) {
    log(exception);
    final ImmutableError error = ImmutableError.builder()
      .code(ErrorCode.REQUEST_NOT_READABLE)
      .message(exception.getMessage())
      .build();
    return CommonController.toErrorResponse(List.of(error));
  }

  @ExceptionHandler(Exception.class)
  private ResponseEntity<ErrorResponse> handleException(Exception exception) {
    log(exception);
    final ImmutableError error = ImmutableError.builder()
      .code(ErrorCode.INTERNAL_SERVER_ERROR)
      .message(ErrorCode.INTERNAL_SERVER_ERROR.getMessage())
      .build();

    return CommonController.toErrorResponse(List.of(error), HttpStatus.INTERNAL_SERVER_ERROR);
  }

  private <S, T extends S> ResponseEntity<S> widen(ResponseEntity<T> entity) {
    return new ResponseEntity<>(entity.getBody(), entity.getHeaders(), entity.getStatusCode());
  }
}
