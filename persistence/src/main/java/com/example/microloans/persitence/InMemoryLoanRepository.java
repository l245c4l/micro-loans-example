package com.example.microloans.persitence;

import com.example.microloans.business.LoanRepository;
import com.example.microloans.domain.loan.ImmutableLoan;
import com.example.microloans.domain.loan.Loan;
import com.example.microloans.domain.loan.LoanCreationProperties;
import com.example.microloans.shared.Error;
import com.example.microloans.shared.ErrorCode;
import io.vavr.collection.HashMap;
import io.vavr.collection.Map;
import io.vavr.control.Validation;
import org.springframework.stereotype.Repository;

@Repository
public class InMemoryLoanRepository implements LoanRepository {

  private Map<Long, Loan> state = HashMap.empty();

  @Override
  public synchronized Validation<Error, Loan> persist(LoanCreationProperties loanCreationProperties) {
    final Long id = state.keySet().toSortedSet().lastOption().map(lastId -> lastId + 1).getOrElse(1L);

    final Loan loan = ImmutableLoan.builder()
      .initialTerm(loanCreationProperties.initialTerm())
      .amount(loanCreationProperties.amount())
      .createdAt(loanCreationProperties.createdAt())
      .extended(loanCreationProperties.extended())
      .id(id)
      .build();

    state = state.put(id, loan);

    return Validation.valid(loan);
  }

  @Override
  public synchronized Validation<Error, Loan> update(Loan loan) {
    state = state.put(loan.id(), loan);

    return Validation.valid(loan);
  }

  @Override
  public synchronized Validation<Error, Loan> findExisting(Long id) {
    return state.get(id).toValid(ErrorCode.LOAN_DOES_NOT_EXIST.asError(id));
  }

  public void cleanDatabase() {
    state = HashMap.empty();
  }
}
