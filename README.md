# Micro loans application

##Run
```
./gradlew bootRun
```

##Tests
```
./gradlew test
```

##Sample requests

### Apply for loan
```
curl localhost:8080/api/v1/loan \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -X POST \
  -d '{"amount": "100000", "initialTerm": "PT360H"}'
```

### Extend loan
```
curl localhost:8080/api/v1/loan/1/extend \
  -H "Accept: application/json" \
  -X POST
```
