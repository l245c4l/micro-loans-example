package com.example.microloans.domain.fixtures;

import com.example.microloans.domain.TimeProvider;
import com.example.microloans.domain.loan.ImmutableLoan;
import java.time.Duration;

public class LoanFixtures {
  public static ImmutableLoan someLoan() {
    return ImmutableLoan.builder()
      .id(1L)
      .createdAt(TimeProvider.zonedNow())
      .amount(1500_00L)
      .initialTerm(Duration.ofDays(15))
      .extended(false)
      .build();
  }
}
